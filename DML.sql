-- DML (Data Manipulation Table)
/*
	1. insert
	2. update
	3. delete
*/

-- insert the data in the table
insert into training09.first_table (first_name, email, emp_age, id) value ('giri', 'giri@gmail.com', 25, 1);
insert into training09.first_table (first_name, email, emp_age, id) value ('pasu', 'pasu@gmail.com', 25, 2);
insert into training09.first_table (first_name, email, emp_age, id) value ('guna', 'guna@gmail.com', 22, 3);
insert into training09.first_table (first_name, email, emp_age, id) value ('theeran', 'theeran@gmail.com', 23, 4);

-- update the column data in all rows in the table
update training09.first_table set first_name='pasupathi';

-- update the data in the table
update training09.first_table set first_name='pasupathi' where id=2;

-- delete the entire data from the table
delete from training09.first_table;

-- delete the selected rows from the table
delete from training09.first_table where id = 1;
