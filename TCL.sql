-- TCL (Transaction Control Language)
/*
	COMMIT
	ROLLBACK
	SAVEPOINT
*/
insert into training09.first_table (first_name, email, emp_age, id) value ('giri', 'giri@gmail.com', 25, 1);
insert into training09.first_table (first_name, email, emp_age, id) value ('giri1', 'giri@gmail.com', 25, 1);
savepoint first1;
insert into training09.first_table (first_name, email, emp_age, id) value ('pasu1', 'pasu@gmail.com', 25, 2);
insert into training09.first_table (first_name, email, emp_age, id) value ('pasu', 'pasu@gmail.com', 25, 2);
insert into training09.first_table (first_name, email, emp_age, id) value ('pasu2', 'pasu@gmail.com', 25, 2);
savepoint second2;
insert into training09.first_table (first_name, email, emp_age, id) value ('guna', 'guna@gmail.com', 22, 3);
savepoint third3;
insert into training09.first_table (first_name, email, emp_age, id) value ('theeran', 'theeran@gmail.com', 23, 4);
savepoint fourth4;

rollback to third3;
commit;