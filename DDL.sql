-- DDL (Data Definition Language)
/* 
	1. CREATE
    2. ALTER
    3. DROP
    4. TRUNCATE
*/

-- schema creation
create schema training09;

-- table creation
create table training09.first_table (
id int,
name varchar(30),
age int,
email varchar(50)
);

-- alter table to rename the column
alter table training09.first_table rename column first_name to name;

-- alter table to rename the 2 columns in one Query
alter table training09.first_table rename column name to first_name, rename column age to empage;

-- alter table to add 1 new column
alter table training09.first_table add column emp_height decimal;
alter table training09.first_table add column yza varchar(20);

-- alter table to modify column datatype
alter table training09.first_table modify column emp_height varchar(10);

-- alter table to drop two columns
alter table training09.first_table drop column xyz, drop column yza;

-- clears the table content but preserves structure
truncate table training09.first_table;


