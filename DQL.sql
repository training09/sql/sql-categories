-- DQL (Data Query Language)
/*
	select
*/

-- select all rows and columns
SELECT * FROM training09.first_table;

-- filter the rows but no columns
SELECT * FROM training09.first_table where id = 3;

-- filter the columns but no rows
SELECT first_name FROM training09.first_table;

-- filter the columns & rows
SELECT first_name, email FROM training09.first_table where id = 1;
